let inputTempo = document.getElementById('inputTempo');
let timerC = document.getElementById('timerCountdown');
let testoFine = document.getElementById('timerFinito');
let btnInizio = document.getElementById('btnInizio');
let btnRefresh = document.getElementById('refreshBtn');
let avviato = false;
let btnTema = document.getElementById('displayMode');




function iniziaCountdown() {
    if (inputTempo.value == false) {
        avviato = false;
    } else {
        avviato = !avviato;
        testoFine.classList.add('nascosto');
    }

    if (avviato == true && btnInizio.className == undefined) {
        console.log(btnInizio.className);
        btnInizio.className = 'btnInizio active';
    }

    let valoreInput = inputTempo.value.split(/[:.,;]/);
    let formatoTempo = '00:00:00'.split(/[:.,;]/);
    let numeriRegex = /^[0-9]+$/;

    //dopo aver diviso l'input in un formato hh:mm:ss, controllo che l'input non sia undefinend
    for (let i = 0; i < 3; i++) {
     
        formatoTempo[i] = valoreInput[i];
        let formatoCheck = formatoTempo[i];

        if (formatoCheck != undefined) {

            formatoTempo[i] = valoreInput[i];

            if(inputTempo.value.split('')[i].match(numeriRegex)){btnInizio.disabled = true}

            //Se viene inserito in input qualcosa diverso da un numero, viene automaticamente sostituito con uno 0
            if (!formatoTempo[i].match(numeriRegex)) {

                formatoTempo[i] = formatoCheck.replace(/[^0-9.]/g, '0');
            }



        } else {
            //Nel caso il valore input sia vuoto aggiundo 00 davanti al tempo e rimouvo l'ultimo elemento
            //Cosi in caso l'input sia 5:55 gli 00 non inseriti vanno davanti e non dietro come farebbe, 
            //Risultato 5:55 --> 00:05:55 
            formatoTempo.unshift('00'), formatoTempo.pop()
        }
    }
    console.log(formatoTempo);
    //prende i dati input formato (HH:mm:ss) e li converte in secondi per il countdown
    let valoreInputInSecondi = +formatoTempo[0] * 3600 + +formatoTempo[1] * 60 + +formatoTempo[2];


    let countDown = valoreInputInSecondi;



    if (avviato == true) {
        
        let inizio = setInterval(function () {

            countDown--;

            let ore = parseInt(countDown / 3600);
            let minuti = parseInt(countDown / 60, 10) % 60;
            let secondi = parseInt(countDown % 60, 10);

            ore = ore < 10 ? "0" + ore : ore;
            minuti = minuti < 10 ? "0" + minuti : minuti;
            secondi = secondi < 10 ? "0" + secondi : secondi;

            timerC.innerHTML = ore + ":" + minuti + ":" + secondi;

            let somma = ore + minuti + secondi;

            if ((countDown <= 0 && somma <= 0) || avviato == false) {
                avviato = false
                testoFine.classList.remove('nascosto')
                resetTimer(valoreInputInSecondi, countDown, timerC, testoFine, inizio)
                clearInterval(inizio)
            }

        }
            , 1000);
    }

    else if (avviato == false) {
        resetTimer(valoreInputInSecondi, countDown, timerC, testoFine)
    }
}


function resetTimer(valoreInputInSecondi, countDown, timerC, testoFine, inizio) {
    valoreInputInSecondi = 0;
    countDown = valoreInputInSecondi;
    timerC.innerText = '00:00:00';
    clearInterval(inizio);
    inputTempo.value = '';
    //testoFine.classList.add('nascosto')
}

function azzeraTimer() {
    avviato = false;
    timerC.innerHTML = '00:00:00'
}




//Applica il tema scuro quando viene cliccato il bottone
btnTema.addEventListener('click', e => {
    e.preventDefault();

    let elementiDom = document.querySelectorAll('*');

    elementiDom.forEach((elemento, i) => {
        elementiDom[i].classList.toggle('darkMode')
    });

    //  document.body.classList.toggle('darkMode')
});

function bloccaInserimento()
{



}


inputTempo.addEventListener('keyup', e =>  {
    e.preventDefault();

    let inputTempoHTML = inputTempo.value;

    if(inputTempoHTML == '' ||  inputTempoHTML == undefined ||  inputTempoHTML == null)
    {
        btnInizio.disabled = true; btnInizio.style = 'hidden'
    }else{
        btnInizio.disabled = false
        btnInizio.style = 'hidden'
    }

})



